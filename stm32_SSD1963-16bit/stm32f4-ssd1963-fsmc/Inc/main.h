/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define STM_LED1_Pin GPIO_PIN_0
#define STM_LED1_GPIO_Port GPIOC
#define FCMC_RESET_Pin GPIO_PIN_5
#define FCMC_RESET_GPIO_Port GPIOC
#define LIGHT_Pin GPIO_PIN_0
#define LIGHT_GPIO_Port GPIOB
#define DSP_RS_Pin GPIO_PIN_13
#define DSP_RS_GPIO_Port GPIOD
#define STM_LED2_Pin GPIO_PIN_3
#define STM_LED2_GPIO_Port GPIOD
/* USER CODE BEGIN Private defines */
#define Clock8x7		&Dmd8x7Clock
#define Clock13x20		&Dmd13x20Clock
#define Mono9			&FreeMono9pt7b
#define Mono12			&FreeMono12pt7b
#define Mono18			&FreeMono18pt7b
#define Mono24			&FreeMono24pt7b
#define MonoBold9		&FreeMonoBold9pt7b
#define MonoBold12		&FreeMonoBold12pt7b
#define MonoBold18		&FreeMonoBold18pt7b
#define MonoBold24		&FreeMonoBold24pt7b
#define MonoBoldOblique9	&FreeMonoBoldOblique9pt7b
#define MonoBoldOblique12	&FreeMonoBoldOblique12pt7b
#define MonoBoldOblique18	&FreeMonoBoldOblique18pt7b
#define MonoBoldOblique24	&FreeMonoBoldOblique24pt7b
#define MonoOblique9		&FreeMonoOblique9pt7b
#define MonoOblique12		&FreeMonoOblique12pt7b
#define MonoOblique18		&FreeMonoOblique18pt7b
#define MonoOblique24		&FreeMonoOblique24pt7b
#define Sans9			&FreeSans9pt7b
#define Sans12			&FreeSans12pt7b
#define Sans18			&FreeSans18pt7b
#define Sans24			&FreeSans24pt7b
#define SansBold9		&FreeSansBold9pt7b
#define SansBold12		&FreeSansBold12pt7b
#define SansBold18		&FreeSansBold18pt7b
#define SansBold24		&FreeSansBold24pt7b
#define SansBoldOblique9	&FreeSansBoldOblique9pt7b
#define SansBoldOblique12	&FreeSansBoldOblique12pt7b
#define SansBoldOblique18	&FreeSansBoldOblique18pt7b
#define SansBoldOblique24	&FreeSansBoldOblique24pt7b
#define SansOblique9		&FreeSansOblique9pt7b
#define SansOblique12		&FreeSansOblique12pt7b
#define SansOblique18		&FreeSansOblique18pt7b
#define SansOblique24		&FreeSansOblique24pt7b
#define Serif9			&FreeSerif9pt7b
#define Serif12			&FreeSerif12pt7b
#define Serif18			&FreeSerif18pt7b
#define Serif24			&FreeSerif24pt7b
#define SerifBold9		&FreeSerifBold9pt7b
#define SerifBold12		&FreeSerifBold12pt7b
#define SerifBold18		&FreeSerifBold18pt7b
#define SerifBold24		&FreeSerifBold24pt7b
#define SerifBoldItalic9	&FreeSerifBoldItalic9pt7b
#define SerifBoldItalic12	&FreeSerifBoldItalic12pt7b
#define SerifBoldItalic18	&FreeSerifBoldItalic18pt7b
#define SerifBoldItalic24	&FreeSerifBoldItalic24pt7b
#define SerifItalic9		&FreeSerifItalic9pt7b
#define SerifItalic12		&FreeSerifItalic12pt7b
#define SerifItalic18		&FreeSerifItalic18pt7b
#define SerifItalic24		&FreeSerifItalic24pt7b
#define SevenSegNum		&FreeSevenSegNum
#define Org			&Org_01
#define Pixel			&Picopixel
#define Tiny3			&Tiny3x3a2pt7b
#define Thumb			&TomThumb
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
